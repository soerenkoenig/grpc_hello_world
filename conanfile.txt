[requires]
grpc/1.37.1

[generators]
cmake
cmake_find_package_multi

[imports]
bin, *.* -> ./bin
lib, *.dylib* -> ./bin
lib, *.so -> ./bin