# GRPC Hello world demo

## Build with CLion on Windows with VS2019 Compiler

Install CLion: https://www.jetbrains.com/toolbox-app/

(Install plugin for conan File -> Settings -> Plugins)

Install Choco: https://chocolatey.org/install

Use choco to install python:

    choco install python

Use choco to install pip:

    choco install pip

use pip to install conan

    pip3 install conan

Create conan profile files in  ~/.conan/profiles for VS2019 release and debug

vs2019_Debug:

    [settings]
    os=Windows
    os_build=Windows
    arch=x86_64
    arch_build=x86_64
    compiler=Visual Studio
    compiler.version=16
    build_type=Debug
    [options]
    [build_requires]
    [env]

vs2019_Release:

    [settings]
    os=Windows
    os_build=Windows
    arch=x86_64
    arch_build=x86_64
    compiler=Visual Studio
    compiler.version=16
    build_type=Release
    [options]
    [build_requires]
    [env]

- load project in CLion

- match CMake Configurations (Debug/Release) with Conan Profiles (vs2019_Release/vs2019_Debug) in Conan plugin setting.

-  Open Configuration for conan and add install args: --build missing

- Reload Cmake (Enable auto reload)

- Compile and Run server and client app.

- In case of 32/64 bit linkage issues: Check selected CMake Toolchain: Visual Studio / amd64







