#include <grpc++/grpc++.h>
#include "helloworld.grpc.pb.h"

#include <iostream>
#include <memory>

class GreeterClient
{
public:
    explicit GreeterClient(const std::shared_ptr<grpc::Channel> &channel)
            : stub_(helloworld::Greeter::NewStub(channel))
    {
    }

    // Assembles the client's payload, sends it and presents the response back
    // from the server.
    std::string SayHello(const std::string &user)
    {
        std::cout << "send a request SayHello with user: " << user << std::endl;
        // Data we are sending to the server.
        helloworld::HelloRequest request;
        request.set_name(user);
        // Container for the data we expect from the server.
        helloworld::HelloReply reply;
        // Context for the client. It could be used to convey extra information to
        // the server and/or tweak certain RPC behaviors.
        grpc::ClientContext context;
        // The actual RPC.
        grpc::Status status = stub_->SayHello(&context, request, &reply);
        // Act upon its status.
        if (status.ok())
        {
            std::cout << "reply succeeded" << std::endl;
            return reply.message();
        } else
        {
            std::cout << status.error_code() << ": " << status.error_message()
                      << std::endl;
            return "RPC failed";
        }
    }

private:
    std::unique_ptr<helloworld::Greeter::Stub> stub_;
};

int main(int, char **)
{
    // Instantiate the client. It requires a channel, out of which the actual RPCs
    // are created. This channel models a connection to an endpoint (in this case,
    // localhost at port 50051). We indicate that the channel isn't authenticated
    // (use of InsecureChannelCredentials()).
    GreeterClient greeter(grpc::CreateChannel(
            "localhost:50051", grpc::InsecureChannelCredentials()));
    std::string user("world");
    std::string reply = greeter.SayHello(user);
    std::cout << "Greeter received: " << reply << std::endl;
    return 0;
}
